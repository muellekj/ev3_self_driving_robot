#!/usr/bin/env python3

from time import sleep
from ev3dev2.motor import LargeMotor, OUTPUT_C, OUTPUT_B
from ev3dev2.sensor import INPUT_1, INPUT_2, INPUT_3, INPUT_4
from ev3dev2.sensor.lego import GyroSensor, LightSensor, UltrasonicSensor
from datetime import datetime

ROBOT_SPEED = 20
LIGHT__LEVEL_ERROR = 10
BLACK_LEVEL = 37
ROTATION_MULTIPLIER = 0.5
MAX_CURVE = 3

class Robot:
    def __init__(self):
        self.left_motor = LargeMotor(OUTPUT_B)
        self.right_motor = LargeMotor(OUTPUT_C)
        self.light_sensor_left = LightSensor(INPUT_2)
        self.light_sensor_right = LightSensor(INPUT_3)
        self.distance_sensor = UltrasonicSensor(INPUT_4)
        self.gyro_sensor = GyroSensor(INPUT_1)
        self.finished = False
        self.start_time = None

    def start(self):
        self.start_time = datetime.now()

    def check_for_obstacle(self):
        return self.distance_sensor.distance_centimeters <= 8

    def drive(self):
        # Checks if there is an object infront (8cm) of the ultra sonic sensor, if yes it turns the motors off
        if self.check_for_obstacle():
            return self.stop()

        if (datetime.now() - self.start_time).total_seconds() > 5:
            if self.sensor_left_too_dark() and self.sensor_right_too_dark():
                self.finished = True
                return self.stop()

        self.gyro_sensor.reset()
        angle = 0
        while True:
            deviation = self.get_left_light_level() - self.get_right_light_level()

            if self.check_for_obstacle():
                break

            if deviation > LIGHT__LEVEL_ERROR:
                self.left_motor.on(ROBOT_SPEED * ROTATION_MULTIPLIER)
                self.right_motor.on(ROBOT_SPEED * 0.1)
                self.is_straight = False
            elif deviation < -LIGHT__LEVEL_ERROR:
                self.left_motor.on(ROBOT_SPEED * 0.1)
                self.right_motor.on(ROBOT_SPEED * ROTATION_MULTIPLIER)
                self.is_straight = False

            if -LIGHT__LEVEL_ERROR <= deviation <= LIGHT__LEVEL_ERROR and not self.sensor_right_too_dark() and not self.sensor_left_too_dark():
                break
            if self.sensor_right_too_dark() and self.sensor_right_too_dark() and (self.gyro_sensor.angle - angle) < MAX_CURVE:
                break

            sleep(0.05)

        self.left_motor.on(ROBOT_SPEED)
        self.right_motor.on(ROBOT_SPEED)

        return True

    def sensor_right_too_dark(self):
        return self.get_right_light_level() <= BLACK_LEVEL
    
    def sensor_left_too_dark(self):
        return self.get_left_light_level() <= BLACK_LEVEL


    def get_left_light_level(self):
        return self.light_sensor_left.reflected_light_intensity + 6

    def get_right_light_level(self):
        return self.light_sensor_right.reflected_light_intensity
    def stop(self):
        self.left_motor.off()
        self.right_motor.off()
        return False