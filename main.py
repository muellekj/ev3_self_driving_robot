#!/usr/bin/env python3

from time import sleep
from drivingrobot import Robot
from ev3dev2.button import Button


robot = Robot()
robot.start()

while True:
    while not robot.finished:
        robot.drive()
        sleep(0.05)

    sleep(0.01)


print('Finished')

    
